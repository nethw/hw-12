import PySimpleGUI as sg
from threading import Thread
import socket
import requests

class Rule:
    def __init__(self, name = "New rule", iip = "127.0.0.1", iipt = 12333, eip = "140.82.112.4", eipt = 80, pos = 0):
        self.waiting = False
        self.name = name
        self.iipt = iipt
        self.eipt = eipt
        self.iip = iip
        self.eip = eip
        self.pos = pos

class Translator:
    def __init__(self, iip, iipt, eip, eipt):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((iip, iipt))
        self.socket.listen(1)
        while True:
            tmpSocket, _ = self.socket.accept()
            tmpSocket.recv(1024)
            page = requests.get(f"http://{eip}:{eipt}", verify=False)
            dpage = page.content.decode(page.encoding)
            tmpSocket.send(bytes(f"HTTP/1.1 200 OK\r\nContent-Length: {len(dpage)}\r\n\r\n{dpage}", "utf-8"))
            tmpSocket.close()

    def __del__(self):
        self.socket.close()

trds = []
rules = [Rule()]

def buildUI():
    return [
        [sg.Text("Name", (20, 1), justification="center", background_color="orange", text_color="black"),
         sg.Text("Local IP", (20, 1), justification="center", background_color="orange", text_color="black"),
         sg.Text("Local port", (20, 1), justification="center", background_color="orange", text_color="black"),
         sg.Text("Server IP", (20, 1), justification="center", background_color="orange", text_color="black"),
         sg.Text("Server port", (20, 1), justification="center", background_color="orange", text_color="black")],
        [sg.InputText(rules[-1].name, (20, 1), key=f"name|{rules[-1].pos}"), sg.InputText(rules[-1].iip, (20, 1), key=f"iip|{rules[-1].pos}"),
        sg.InputText(rules[-1].iipt, (20, 1), key=f"iipt|{rules[-1].pos}"), sg.InputText(rules[-1].eip, (20, 1), key=f"eip|{rules[-1].pos}"),
        sg.InputText(rules[-1].eipt, (20, 1), key=f"eipt|{rules[-1].pos}")],
        *[[sg.Text(rules[i].name, (20, 1), justification="center", background_color= "lightgrey", text_color="black"), 
        sg.Text(rules[i].iip, (20, 1), justification="center", background_color= "lightgrey", text_color="black"),
        sg.Text(rules[i].iipt, (20, 1), justification="center", background_color= "lightgrey", text_color="black"),
        sg.Text(rules[i].eip, (20, 1), justification="center", background_color= "lightgrey", text_color="black"),
        sg.Text(rules[i].eipt, (20, 1), justification="center", background_color= "lightgrey", text_color="black")]
        for i in range(len(rules) - 1)],
        [sg.Button("Save rule", expand_x=True, button_color="black"), sg.Button("Run", expand_x=True, button_color="black")]
    ]
window = sg.Window("PT App", buildUI(), background_color="orange")

while True:
    button, rparts = window.read(100)
    if rparts is not None:
        for rpart, val in rparts.items():
            rpn, rpos = rpart.split("|")
            rpos = int(rpos)
            if rpn == "name":
                if rules[rpos].name != val:
                    rules[rpos].name = val
                    rules[rpos].waiting = True
            if rpn == "iipt":
                try:
                    if rules[rpos].iipt != int(val):
                        rules[rpos].iipt = int(val)
                        rules[rpos].waiting = True
                except:
                    continue
            if rpn == "eipt":
                try:
                    if rules[rpos].eipt != int(val):
                        rules[rpos].eipt = int(val)
                        rules[rpos].waiting = True
                except:
                    continue
            if rpn == "iip":
                if rules[rpos].iip != val:
                    rules[rpos].iip = val
                    rules[rpos].waiting = True
            if rpn == "eip":
                if rules[rpos].eip != val:
                    rules[rpos].eip = val
                    rules[rpos].waiting = True
    if button == "Run":
        for i in range(len(rules)):
            if rules[i].waiting:
                rules[i].waiting = False
                def launch():
                    Translator(rules[i].iip, rules[i].iipt, rules[i].eip, rules[i].eipt)
                trds.append(Thread(target=launch))
                trds[-1].start()
    if button in (None, "Exit"):
        window.close()
        break
    if button == "Save rule":
        rules.append(Rule(pos = len(rules)))
        window.close()
        window = sg.Window("PT App", buildUI(), background_color="orange")
for trd in trds:
    trd.join()