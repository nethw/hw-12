import json

class RIP:
    def __init__(self, address, connections):
        self.address = address
        self.connections = set(connections)
        self.rrt = {connection: connection for connection in self.connections}
        self.cls = {connection: 1 for connection in self.connections}

step = 1
df = True
f = open("result.txt", "w")
input = json.load(open('input.json'))
rs = [RIP(rname, input[rname]) for rname in input]

while df:
    df = False
    for i in range(len(rs)):
        for j in range(len(rs)):
            if rs[i].address == rs[j].address:
                continue
            for ncn in rs[i].connections:
                ndv = 100000000
                if ncn in rs[j].cls:
                    ndv = rs[j].cls[ncn]
                nnndv = False
                if (rs[j].address not in rs[i].cls or 1 + ndv < rs[i].cls[rs[j].address]) and 1 + ndv < 100000000:
                    rs[i].cls[rs[j].address] = 1 + ndv
                    rs[i].rrt[rs[j].address] = ncn
                    nnndv = True
                df |= nnndv
        f.write(f'Simulation step {step} of router {rs[i].address}\n')
        f.write(f'{"[Source IP]":15} {"[Destination IP]":15} {"[Next Hop]":15} {"[Metric]":10}\n')
        for target in rs[i].cls:
            f.write(f'{rs[i].address:15} {target:16} {rs[i].rrt[target]:22} {str(rs[i].cls[target]):1}\n')
        f.write("\n")
    step += 1
for rt in rs:
    f.write(f'Final state of router {rt.address} table:\n')
    f.write(f'{"[Source IP]":15} {"[Destination IP]":15} {"[Next Hop]":15} {"[Metric]":10}\n')
    for target in rt.cls:
        f.write(f'{rt.address:15} {target:16} {rt.rrt[target]:22} {str(rt.cls[target]):1}\n')
    f.write("\n")